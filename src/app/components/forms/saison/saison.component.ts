import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { SaisonServiceService } from 'src/app/services/saison-service.service';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-saison',
  templateUrl: './saison.component.html',
  styleUrls: ['./saison.component.css']
})
export class SaisonComponent implements OnInit {

  model!: NgbDateStruct;
  saisonForm!: FormGroup;
  create = true;

  constructor(
    private activeModal: NgbActiveModal,
    private sharedService: SharedService,
    private formBuilder: FormBuilder,
    private saisonService: SaisonServiceService) { }

  ngOnInit(): void {


    this.saisonForm = this.formBuilder.group({
      id: [""],
      name: ["",Validators.required],
      startDate: [""],
      endDate: [""],
    });

  }

  close(value: any) {
    this.activeModal.close({
      value,
    });
  }

  update() {

  }

  createSaison() {
    console.log(this.saisonForm.value)
    this.saisonService.create(this.saisonForm.value).subscribe({
      next: (result) => {
        console.log(result);
        this.activeModal.close({
          value: true
        });
      },
      error: (error) => {
        console.log(error);
      }
    });
  }

}
