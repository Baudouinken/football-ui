import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/forms/login/login.component';
import { HomeComponent } from './components/home/home/home.component';
import { ListPlayersGoalComponent } from './components/list-players-goal/list-players-goal.component';

const routes: Routes = [
  { path: 'list-players-goals', component: ListPlayersGoalComponent },
  { path: '',      component: HomeComponent },
  { path: '*',      component: HomeComponent },
  { path: 'login', component: LoginComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
