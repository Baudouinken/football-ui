import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MatchResultService {

  readonly MATCH_RS_API_URL: string = `${environment.apiUrl}/matchresults`;
  headers = new HttpHeaders({ "Content-Type": "application/json" });

  constructor(private httpClient: HttpClient) { }


  addMatchResult(any: any): Observable<any> {
    const   any_read_url = `${this.MATCH_RS_API_URL}/`;
    return this.httpClient.post<any>(  any_read_url,   any, {headers: this.headers});
  }

  // UPDATE
  update(any: any): Observable<any> {
    const   any_read_url = `${this.MATCH_RS_API_URL}`;
    return this.httpClient.put<any>(any_read_url, any, {headers: this.headers});
  }

  // DELETE
  delete(id: string): Observable<any> {
    const property_read_url = `${this.MATCH_RS_API_URL}/${id}`;
    return this.httpClient.delete<any>(property_read_url, {headers: this.headers});
  }

}
