import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListPlayersGoalComponent } from './list-players-goal.component';

describe('ListPlayersGoalComponent', () => {
  let component: ListPlayersGoalComponent;
  let fixture: ComponentFixture<ListPlayersGoalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListPlayersGoalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPlayersGoalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
