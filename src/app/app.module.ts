import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgbAlertModule, NgbDatepickerModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './components/home/home/home.component';
import { ToastrModule } from 'ngx-toastr';
import { LocationStrategy, HashLocationStrategy, JsonPipe, registerLocaleData } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { SisonDetailComponent } from './components/sison-detail/sison-detail.component';
import { MatchDetailsComponent } from './components/match-details/match-details.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SaisonComponent } from './components/forms/saison/saison.component';
import { MatchComponent } from './components/forms/match/match.component';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatNativeDateModule, MatPseudoCheckboxModule, MatRippleModule } from '@angular/material/core';
import {MatInputModule} from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { NgxMatDatetimePickerModule, NgxMatNativeDateModule, NgxMatTimepickerModule } from '@angular-material-components/datetime-picker';
import { PersonComponent } from './components/forms/person/person.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { LoginComponent } from './components/forms/login/login.component';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';

import localeFr from '@angular/common/locales/en';
import { ListPlayersGoalComponent } from './components/list-players-goal/list-players-goal.component';

registerLocaleData(localeFr);

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SisonDetailComponent,
    MatchDetailsComponent,
    SaisonComponent,
    MatchComponent,
    PersonComponent,
    LoginComponent,
    ListPlayersGoalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-bottom-right',
      progressAnimation: 'increasing'
    }),
    NgbDatepickerModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    MatPseudoCheckboxModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    NgxMatDatetimePickerModule,
    NgxMatTimepickerModule,
    NgxMatNativeDateModule,
    MatToolbarModule,
    MatCardModule,
    MatIconModule
  ],
  providers: [
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    { provide: LOCALE_ID, useValue: 'en-EN' },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
