# football-ui



## Getting started

It is a Backend-API of a application that allows a group or a soccer organization to have a traceability on their different soccer seasons.

In one season you can create several matches and for each match you can add the players that played that day as well as the number of goals and wins.

## Links

- [ ] Dev : http://ken-root.tech/dev/football-ui/football-ui/#/


## Backend
- [ ] Repos link: https://gitlab.com/Baudouinken/football-api

- [ ] Swagger: 




## Deploy
The App is doplyed on a linux root-server with gitlab-runners through a gitlab.yml file
