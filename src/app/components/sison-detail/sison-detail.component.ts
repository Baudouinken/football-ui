import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { MatchService } from 'src/app/services/match-service.service';
import { SaisonServiceService } from 'src/app/services/saison-service.service';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-sison-detail',
  templateUrl: './sison-detail.component.html',
  styleUrls: ['./sison-detail.component.css']
})
export class SisonDetailComponent implements OnInit {

  matchIds: string[] = [];

  matchs: any[] = [];

  signedIn = false;

  constructor(
    private sharedService: SharedService,
    private matchService: MatchService,
    private saisonService: SaisonServiceService,
    private localStorageService: LocalStorageService,
    private authService: AuthService
    ) { }

  ngOnInit(): void {

    this.signedIn = this.authService.isLoggedIn;

    // handel when saison ist selected
    this.sharedService.getMatchIds().subscribe(ids => {

      this.matchIds = ids;
      this.matchs = [];

      this.refreshMatchs();

    });


    // When match created reload the list of Matchs
    this.sharedService.getCreateMatch().subscribe(created => {
      this.matchIds = [];
      this.matchs = [];

      this.refreshMatchs();
    });
  }

  changeColor(id: string) {
    this.matchs.map(e => e.selected = false);

    this.matchs.filter(e => e.id == id)[0].selected = true;

  }

  refreshMatchs() {

    let saisonId:any = this.localStorageService.getSaisonId();

    this.saisonService.getMatchs(saisonId).subscribe({
      next: (result) => {
        this.matchs = result;
        // set first match of first saison
        this.setMatch(this.matchs[0])

      },
      error: (error) => {

      }
    });
  }

  // update the form with the selected match
  setMatch(match: any) {
    console.log(match)
    this.sharedService.setMatch(match);
  }

  // delte match
  deleteMatch(matchId: string) {
    this.matchIds = [];
    this.matchs = [];
    this.matchService.delete(matchId).subscribe(e => {
      console.log(e)
      this.refreshMatchs();
    });
  }

}
