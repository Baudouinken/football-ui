import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SisonDetailComponent } from './sison-detail.component';

describe('SisonDetailComponent', () => {
  let component: SisonDetailComponent;
  let fixture: ComponentFixture<SisonDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SisonDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SisonDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
