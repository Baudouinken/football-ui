import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  private saisonSubject = new Subject<any>();
  private matchSubject = new Subject<any>();
  private addMatchSubject = new Subject<any>();
  private createdMatchSubject = new Subject<any>();
  private updateToolbarSubject = new Subject<any>();

  constructor() { }

  setMatchIds(saisonId: string[]) {
    this.saisonSubject.next(saisonId);
  }

  getMatchIds(): Observable<string[]> {
    return this.saisonSubject.asObservable();
  }

  setMatch(match: any) {
    this.matchSubject.next(match);
  }

  getMatch(): Observable<any> {
    return this.matchSubject.asObservable();
  }

  setAddMatch(val: any) {
    this.addMatchSubject.next(val);
  }

  getAddMatch(): Observable<any> {
    return this.addMatchSubject.asObservable();
  }

  setCreatedatch(val: any) {
    this.createdMatchSubject.next(val);
  }

  getCreateMatch(): Observable<any> {
    return this.createdMatchSubject.asObservable();
  }

  setUpdateToolbar(val: any) {
    this.updateToolbarSubject.next(val);
  }

  getUpdateToolbar(): Observable<any> {
    return this.updateToolbarSubject.asObservable();
  }

}
