import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';

import { ToastrService } from "ngx-toastr";
import { of, switchMap } from "rxjs";
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SaisonServiceService } from 'src/app/services/saison-service.service';
import { SharedService } from 'src/app/services/shared.service';
import { SaisonComponent } from '../../forms/saison/saison.component';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { AuthService } from 'src/app/services/auth.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  saisons: any[] = [];
  loading = true;
  pushed = false;
  closeResult = "";
  searchInputControl: FormControl;
  signedIn = false;

  constructor(
    private saisonService: SaisonServiceService,
    private modalService: NgbModal,
    private toaster: ToastrService,
    private sharedService: SharedService,
    private authService: AuthService,
    private localStorageService: LocalStorageService
  ) {
    this.searchInputControl = new FormControl();
  }

  ngOnInit(): void {

    this.signedIn = this.authService.isLoggedIn;

    this.saisonService.getAll().subscribe({
      next: (result) => {

        this.saisons = result;

        // select the first saison
        this.sharedService.setMatchIds(this.saisons[0].matchsIds);
        // show add button
        this.pushed = true;

        console.log(this.saisons);
      },
      error: (error) => {
        console.log(error);
      },
    });
  }

  // when saison clicked
  getSaison(matchsIds: string[], saisonId: string) {
    this.pushed = true;
    this.localStorageService.setSaisonId(saisonId);
    this.sharedService.setMatchIds(matchsIds);
  }

  // Call Modal to add saison
  addSaison() {
    const modalRef = this.modalService.open(SaisonComponent, {
      ariaLabelledBy: "modal-basic-title",
      size: "lg",
      keyboard: false,
      backdrop : 'static'
    });

    modalRef.result.then(
      (result) => {
        this.closeResult = `Closed with: ${result}`;
        if(result.value===true) {
          console.log(45)
          this.ngOnInit();
        }
      }
    );
  }

  changeColor(id: string) {
    this.saisons.map(e => e.selected = false);

    this.saisons.filter(e => e.id == id)[0].selected = true;
  }

  // Make form empty to add match
  addMatch() {
    this.sharedService.setAddMatch(true);
  }

  // delete saison
  deleteSaison(matchId: string) {
    this.saisons = [];
    this.saisonService.delete(matchId).subscribe(e => {
      console.log(e)
      this.ngOnInit();
    });
  }

}
