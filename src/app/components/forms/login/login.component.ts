import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/services/auth.service';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form!: FormGroup;
  flag: boolean = true;
  loading!: boolean;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private toastr: ToastrService,
    private router: Router,
    private sharedService: SharedService
  ) { }

  ngOnInit(): void {
    this.checkUserAuthState();
    this.form = this.fb.group({
      username: ["", [Validators.required]],
      password: ["", [Validators.required, Validators.minLength(6)]]
    });
  }

  private checkUserAuthState() {
    if(this.authService.isLoggedIn) {
      this.router.navigate(['']);
    }
  }

  saveDetails() {

    this.loading = true;

    this.authService.signIn(this.form.value).subscribe({
      next: (response) => {
        this.loading = false;
        this.toastr.success("Successfully logged in!", "Success");

        localStorage.setItem("token", response.token);
        localStorage.setItem("username", response.username);
        localStorage.setItem("signedIn", response.signedIn);
      },
      error: (error) => {
        this.toastr.error(error.error.message, "Error");
      },
      complete: () => {
        this.sharedService.setUpdateToolbar(true);
        this.router.navigate(["/"]);
      }
    });
  }

}

