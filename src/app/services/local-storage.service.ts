import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }

  public setSaisonId(id: string) {
    localStorage.setItem('saisonId', id);
  }

  public getSaisonId() {
    return localStorage.getItem('saisonId');
  }

}
