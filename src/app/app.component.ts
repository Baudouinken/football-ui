import { Component } from '@angular/core';
import { AuthService } from './services/auth.service';
import { SharedService } from './services/shared.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'football-ui';
  signedIn = false;

  constructor(
    private authService: AuthService,
    private sharedService: SharedService
  ) { }

  ngOnInit(): void {
    this.signedIn = this.authService.isLoggedIn;

    this.sharedService.getUpdateToolbar().subscribe(e => {

      location.reload();
    });
  }

  logout() {
    console.log(12)
    this.authService.doLogout();
  }
}
