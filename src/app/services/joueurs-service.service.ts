import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class JoueursServiceService {

  readonly JOUEURS_API_URL: string = `${environment.apiUrl}/persons`;
  headers = new HttpHeaders({ "Content-Type": "application/json" });

  constructor(private httpClient: HttpClient) { }

  // GET ONE
  create(  any:   any): Observable<  any> {
    const   any_read_url = `${this.JOUEURS_API_URL}`;
    return this.httpClient.post<  any>(  any_read_url,   any, {headers: this.headers});
  }

  // GET ONE
  getAll(): Observable<  any[]> {
    const   any_read_url = `${this.JOUEURS_API_URL}`;
    return this.httpClient.get<  any[]>(  any_read_url, {headers: this.headers});
  }

  // GET All With Goals
  getAllWithGoals(): Observable<any[]> {
    const   any_read_url = `${this.JOUEURS_API_URL}/goals`;
    return this.httpClient.get<  any[]>(  any_read_url, {headers: this.headers});
  }

  // GET ONE
  getOne(id: string): Observable<  any> {
    const   any_read_url = `${this.JOUEURS_API_URL}/${id}`;
    return this.httpClient.get<  any>(  any_read_url, {headers: this.headers});
  }

  // DELETE
  delete(id: string): Observable<any> {
    const property_read_url = `${this.JOUEURS_API_URL}/${id}`;
    return this.httpClient.delete<any>(property_read_url, {headers: this.headers});
  }

  search(filter: any): Observable<  any[]> {
    const   any_read_url = `${this.JOUEURS_API_URL}/search`;
    return this.httpClient.post<any[]>(  any_read_url, filter, {headers: this.headers});
  }

    // UPDATE
    update(id: string,   any: any): Observable<any> {
      const   any_read_url = `${this.JOUEURS_API_URL}/${id}`;
      return this.httpClient.put<any>(  any_read_url,   any, {headers: this.headers});
    }


}
