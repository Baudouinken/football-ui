import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import { NgbDateStruct, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JoueursServiceService } from 'src/app/services/joueurs-service.service';
import { MatchResultService } from 'src/app/services/match-result.service';
import { MatchService } from 'src/app/services/match-service.service';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})
export class PersonComponent implements OnInit {

  toppings = new FormControl('');
  toppingList: any[] = [];

  @Input()
  matchId!: string;

  @Input()
  existing = false;

  @Input()
  isUpdate = false;

  @Input()
  lockPersonFields = false;

  @Input()
  matchResult: any;

  model!: NgbDateStruct;
  personForm!: FormGroup;
  create = true;

  constructor(
    private activeModal: NgbActiveModal,
    private sharedService: SharedService,
    private formBuilder: FormBuilder,
    private personService: JoueursServiceService,
    private matchService: MatchService,
    private matchResultService: MatchResultService
  ) { }

  ngOnInit(): void {

    this.personForm = this.formBuilder.group({
      id: [""],
      name: ["",Validators.required],
      surname: [""],
      //email: [""]
    });

    if(this.existing) {
      this.personForm = this.formBuilder.group({
        joueurs: this.formBuilder.array([
          this.formBuilder.group({
            id: [""],
            playerId: [""],
            win: [false, Validators.required],
            nombreButs: ["0",Validators.required],
            passeD: ["0",Validators.required]
          })
        ])
      });

      this.personService.getAll().subscribe({
        next: (result) => {
          this.toppingList = result;
          console.log(this.toppingList);
        },
        error: (error) => {
          console.log(error);
        }
      });
    }

    if(this.isUpdate) {
      this.personForm = this.formBuilder.group({
        joueurs: this.formBuilder.array([
          this.formBuilder.group({
            id: [this.matchResult.id],
            playerId: [this.matchResult.joueurId],
            win: [String(this.matchResult.win), Validators.required],
            nombreButs: [this.matchResult.nombreButs, Validators.required],
            passeD: [this.matchResult.passesD, Validators.required]
          })
        ])
      });

    }

  }


  close(value: any) {
    this.activeModal.close({
      value,
    });
  }

  update() {
    console.log(this.personForm.value.joueurs[0])

    let createMatch = {
      id: this.personForm.value.joueurs[0].id,
      win: this.personForm.value.joueurs[0].win,
      nombreButs: this.personForm.value.joueurs[0].nombreButs,
      passesD: this.personForm.value.joueurs[0].passeD
    }

    this.matchResultService.update(createMatch).subscribe({
      next: (result) => {
        console.log(result);
        this.activeModal.close({
          value: true
        });

      },
      error:(error) => {

      }
    });
  }

  createPerson() {

    if(!this.existing) {
      let createMatch = {
        name: this.personForm.get('name')?.value,
        surname: this.personForm.get('surname')?.value,
        //email: this.personForm.get('email')?.value,
      }

      console.log(this.personForm.value)
      this.personService.create(createMatch).subscribe({
        next: (result) => {
          console.log(result);
          this.activeModal.close({
            value: true
          });
        },
        error: (error) => {
          console.log(error);
        }
      });
    } else {
      console.log(this.personForm.value);

      let formArray = this.personForm.get('joueurs') as FormArray;

      formArray.controls.forEach(form => {
        let createMatch = {
          win: form.get('win')?.value,
          nombreButs: form.get('nombreButs')?.value,
          passesD: form.get('passeD')?.value,
          matchId: this.matchId,
          joueurId: form.get('playerId')?.value
        }

        this.matchResultService.addMatchResult(createMatch).subscribe({
          next: (result) => {
            console.log(result);
            this.activeModal.close({
              value: true
            });
          },
          error: (error) => {
            console.log(error);
          }
        });
      });

    }

  }

  public get joueurs(){
    return this.personForm.get('joueurs') as FormArray;
  }

  public addJoueur(){
    const joueur = this.formBuilder.group({
      id: [""],
      playerId: [""],
      //email: [""],
      win: [false, Validators.required],
      nombreButs: ["0",Validators.required],
      passeD: ["0",Validators.required]
    })
    this.joueurs.push(joueur)
  }

  public removeJoueurs(index: number){
      this.joueurs.removeAt(index)
  }

}
