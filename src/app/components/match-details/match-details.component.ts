import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { SharedService } from 'src/app/services/shared.service';
import { MatchService } from 'src/app/services/match-service.service';
import { PersonComponent } from '../forms/person/person.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { JoueursServiceService } from 'src/app/services/joueurs-service.service';
import { AuthService } from 'src/app/services/auth.service';
import { MatchResultService } from 'src/app/services/match-result.service';

@Component({
  selector: 'app-match-details',
  templateUrl: './match-details.component.html',
  styleUrls: ['./match-details.component.css']
})
export class MatchDetailsComponent implements OnInit {

  matchForm!: FormGroup;
  actualMatch: any;
  loaded = false;
  adding = false;
  created = false;
  joueurs: any[] = [];
  matchResults: any[] = [];
  closeResult = '';
  signedIn = false;

  constructor(
    private sharedService: SharedService,
    private formBuilder: FormBuilder,
    private localStorageService: LocalStorageService,
    private matchService: MatchService,
    private modalService: NgbModal,
    private personeService: JoueursServiceService,
    private authService: AuthService,
    private matchResultService: MatchResultService
  ) { }

  ngOnInit(): void {

    this.signedIn = this.authService.isLoggedIn;

    this.matchForm = this.formBuilder.group({
      id: [""],
      name: ["",Validators.required],
      startDate: [""],
      endDate: [""],
      matchType: ["calcio",Validators.required],
    });

    this.sharedService.getMatch().subscribe(match => {
      this.adding = false;
      this.loaded = true;
      this.actualMatch = match;
      this.matchResults = [];
      this.matchResults = this.actualMatch?.matchResults;
      console.log(this.matchResults)
      //this.refreshMatchResults();


      this.joueurs.sort((a: any, b: any) => {
        return  0 - (a.nombreButs > b.nombreButs ? 1 : -1);
      });

      this.matchForm.patchValue(this.actualMatch);
    });

    this.sharedService.getAddMatch().subscribe(match => {

      this.adding = true;
      this.loaded = true;

      this.joueurs = [];
      this.matchForm = this.formBuilder.group({
        id: [""],
        name: ["",Validators.required],
        startDate: [new Date()],
        endDate: [new Date()],
        win: [false],
        matchType: ["calcio",Validators.required],
      });
    });
  }


  createMatch() {

    let createMatch = {
      name: this.matchForm.get('name')?.value,
      startDate: this.matchForm.get('startDate')?.value,
      endDate: this.matchForm.get('endDate')?.value,
      matchType: this.matchForm.get('matchType')?.value,
      saisonId: this.localStorageService.getSaisonId()
    }

    //console.log(createMatch);
    this.matchService.create(createMatch).subscribe({
      next:(result) => {
        //console.log(result);
        this.sharedService.setCreatedatch(true);
      },
      error: (error) => {

        console.log(error);
      }
    });

  }

  createJoueurs(existing: boolean) {
    const modalRef = this.modalService.open(PersonComponent, {
      ariaLabelledBy: "modal-basic-title",
      size: "xl",
      keyboard: false,
      backdrop : 'static',
      animation: true
    });

    modalRef.componentInstance.matchId = this.actualMatch.id;
    modalRef.componentInstance.existing = existing;

    modalRef.result.then(
      (result) => {
        this.closeResult = `Closed with: ${result}`;
        //console.log(result)
        if(result.value===true) {
          console.log('wert')
          this.sharedService.setCreatedatch(true);

          this.matchService.getOne(this.actualMatch.id).subscribe({
            next:(result) => {
              this.adding = false;
              this.loaded = true;
              this.actualMatch = result;
              this.matchResults = [];
              this.matchResults = this.actualMatch?.matchResults;

              this.joueurs.sort((a: any, b: any) => {
                return a.nombreButs - b.nombreButs;
              });

              this.matchForm.patchValue(this.actualMatch);
            },
            error: (error) => {

              console.log(error);
            }
          });
        }
      },
      (reason) => {

      }
    );
  }

  deleteMatchResult(matchResult: any) {
    this.matchResultService.delete(matchResult.id).subscribe( result => {
      // console.log(result)
      this.matchService.getOne(this.actualMatch.id).subscribe({
        next:(result_) => {
          this.adding = false;
          this.loaded = true;
          this.actualMatch = result_;
          this.matchResults = [];
          this.matchResults = this.actualMatch?.matchResults;

          this.joueurs.sort((a: any, b: any) => {
            return a.nombreButs - b.nombreButs;
          });

          this.matchForm.patchValue(this.actualMatch);
        },
        error: (error) => {

          console.log(error);
        }
      });
    })
  }

  updateMatchResult(matchResult: any) {

    const modalRef = this.modalService.open(PersonComponent, {
      ariaLabelledBy: "modal-basic-title",
      size: "xl",
      keyboard: false,
      backdrop : 'static',
      animation: true
    });

    modalRef.componentInstance.matchId = this.actualMatch.id;
    modalRef.componentInstance.existing = true;
    modalRef.componentInstance.lockPersonFields = true;
    modalRef.componentInstance.isUpdate = true;
    modalRef.componentInstance.matchResult = matchResult;

    modalRef.result.then(
      (result) => {
        this.closeResult = `Closed with: ${result}`;
        //console.log(result)
        if(result.value===true) {
          console.log('wert')
          this.sharedService.setCreatedatch(true);

          this.matchService.getOne(this.actualMatch.id).subscribe({
            next:(result) => {
              this.adding = false;
              this.loaded = true;
              this.actualMatch = result;
              this.matchResults = [];
              this.matchResults = this.actualMatch?.matchResults;

              this.joueurs.sort((a: any, b: any) => {
                return a.nombreButs - b.nombreButs;
              });

              this.matchForm.patchValue(this.actualMatch);
            },
            error: (error) => {

              console.log(error);
            }
          });
        }
      },
      (reason) => {

      }
    );

  }

  getPlayer(joueur: any): number {

    return joueur?.matchResults?.filter((e:any) => {
      return e.matchId === this.actualMatch.id;
    })[0]?.nombreButs;
  }


}
