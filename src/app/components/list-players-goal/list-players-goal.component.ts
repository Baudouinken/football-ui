import { Component, OnInit } from '@angular/core';
import { JoueursServiceService } from 'src/app/services/joueurs-service.service';

@Component({
  selector: 'app-list-players-goal',
  templateUrl: './list-players-goal.component.html',
  styleUrls: ['./list-players-goal.component.css']
})
export class ListPlayersGoalComponent implements OnInit {

  playerResults!: any[];
  loading = false;

  constructor(
    private joueurService: JoueursServiceService
  ) { }

  ngOnInit(): void {
    this.loading = true;
    this.playerResults = [];
/*     this.joueurService.getAll().subscribe(j => {
      console.log(j);

      j.forEach(py => {

        let player = {
          name: py.name,
          surname: py.surname,
          goals: this.getGoals(py),
          passesD: this.getPassesD(py),
          wins: this.getWins(py),
        }
        this.playerResults.push(player);
      });

      this.playerResults.sort((a: any, b: any) => {
        return (a.wins - b.wins) || (a.goals - b.goals);
      });

      this.playerResults = this.playerResults.reverse();

      this.loading = false;
      console.log(this.playerResults);
    }); */

    this.joueurService.getAllWithGoals().subscribe(players => {

      this.playerResults = players;

      this.playerResults.sort((a: any, b: any) => {
        return (a.wins - b.wins) || (a.goals - b.goals);
      });

      this.playerResults = this.playerResults.reverse();

      this.loading = false;
      console.log(this.playerResults);
    });

  }

  getGoals(player: any) : number {
    let goals = 0;
    player.matchResults.forEach((re:any) => {
      goals+=re.nombreButs;
    });
    return goals;
  }

  getPassesD(player: any) : number {
    let passes = 0;
    player.matchResults.forEach((re:any) => {
      passes+=re.passesD;
    });
    return passes;
  }

  getWins(player: any) : number {
    let wins = 0;
    player.matchResults.forEach((re:any) => {
      if(re.win===true) {
        wins+=1;
      }
    });
    return wins;
  }

}
